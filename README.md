# OmpTask-Lab6-TreeReduction
Nesse lab temos que aplicar um tipo diferente de distribuição de tarefas.
O *for* junto com o *reduce* é um exemplo simples de distribuição, mas e se quisermos fazer uma operação diferente,
uma não suportada pelo *reduce*? Ou várias operações de uma vez?

Uma opção é dividir as tarefas em blocos onde cada thread calcula um resultado parcial, e depois unir todas as partes.
O problema é quando a "função de agregação" é uma operação lenta, ou seja juntar as partes não será uma tarefa fácil.

Podemos então dividir as tarefas de outra maneira:

Na primeira iteração as tarefas são criadas associadas a pares de valores, produzindo vários resultados parciais.
Em uma segunda iteração juntamos o que foi obtido em novos pares e assim por diante.
Note que na primeira iteração os valores de entrada de uma tarefa são os de índice **i** e **i+1** onde **i** é um
índice divisível por 2, na segunda **i** e **i+2** onde **i** e divisível por 4.

Generalizando, temos que:

Para uma dada iteração **k > 0** operamos com os índices **i** e **i+diff** onde **diff=2^(k-1)**.

No final as tarefas corresponderam a árvore (exemplo de soma de valores): 

![Example n-tree](example.png)

**Importante**: Note que uma tarefa só depende de duas entradas, ou seja a subárvore de resultado *46* pode acabar mesmo
antes da soma de *8* com *19* ser feita.

**Tarefa:**
O programa realiza a multiplicação de várias matrizes aleatórias. Dada uma entrada é gerada várias
matrizes e a saída é a multiplicação de todas elas.

**Entrada:**
```
2 3
```

Uma única linha contendo o número de elementos do vetor e tamanho da matriz aleatória criada em cada posição.

**Saída:**
```
3 1 -1
1 3 1
-1 -3 -1
```

A multiplicação de todas as matrizes geradas.

A implementação serial deste problema está no arquivo *matrix_mul.c*. Utilize *tasks* para paralelizar a função **array_mul**
tomando cuidado com as dependências.

Anything missing? Ideas for improvements? Make a pull request.